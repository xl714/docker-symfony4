#!/bin/bash

composer self-update


source /projects/docker-symfony4/php-fpm/sites.sh

for project in ${!sites[*]}
do
    #server_name=${sites[$project]}

    if [ -d "/projects/$project" ]; then
	    
		#
	    ### ajout des crons ... ne fonctionne pas sur alpine
	    #
		# if [ -f /projects/$project/files-php/crontab.cron ]; then
		#     cat /projects/$project/files-php/crontab.cron > /var/spool/cron/crontabs/root
		# fi
		### Recours au système DMG ... le système D de McGyver
		if [ -f /projects/$project/files-php/simulate-cron.sh ]; then
			chmod o+x /projects/$project/files-php/simulate-cron.sh
		    nohup /projects/$project/files-php/simulate-cron.sh &
		fi

		#
		### compilation des less en css
		#
	    cd /projects/$project/symfony
	    yarn install
	    # en dev
	    nohup  yarn encore dev --watch &
	    # prod  ?
	    # nohup yarn encore prod --watch &

	fi

done

nohup  yarn encore dev --watch &


php-fpm7 -F

#touch /tmp/container-run.log && tail -f /tmp/container-run.log;
