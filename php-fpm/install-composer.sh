#!/bin/sh

# USE:
# sh /projects/docker-symfony4/php-fpm/install-composer.sh

# Check for composer first
composer -v > /dev/null 2>&1
COMPOSER=$?
if [[ $COMPOSER -ne 0 ]]; then

    echo 'Composer is not installed >>> Just do it now !'

    EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
    then
        >&2 echo 'ERROR: Invalid installer signature'
        rm composer-setup.php
        exit 1
    fi

    php composer-setup.php --quiet --install-dir=/usr/bin
    RESULT=$?
    rm composer-setup.php
    exit $RESULT

else
    echo 'Composer is installed >>> Nothing to do'
fi
